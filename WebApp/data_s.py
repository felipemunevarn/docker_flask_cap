#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 29 11:23:52 2021

@author: orc
"""

from statistics import stdev, mean

class Data():
    """
    This class test dataframe iris
    """
    def __init__(self, df, column="sepal_length", bins=16):
        '''
        Init variables

        Parameters
        ----------
        df : TYPE
            DESCRIPTION.
        column : TYPE, optional
            DESCRIPTION. The default is "sepal_length".
        bins : TYPE, optional
            DESCRIPTION. The default is 16.

        Returns
        -------
        None.

        '''
        self.df = df
        self.column = column
        self.bins = bins

    def resum_data(self):
        '''
        Calculate basic statistics

        Returns
        -------
        max_f : TYPE
            DESCRIPTION.
        min_f : TYPE
            DESCRIPTION.
        sd_f : TYPE
            DESCRIPTION.
        mean_f : TYPE
            DESCRIPTION.

        '''
        df = self.df
        column = self.column

        max_f = df[column].max()
        min_f = df[column].min()
        sd_f = round(stdev(df[column]), 1)
        mean_f = round(mean(df[column]), 1)


        return max_f, min_f, sd_f, mean_f

    def hist_data(self):
        '''
        Plot selected column

        Returns
        -------
        None.

        '''
        df = self.df
        column = self.column
        bins = self.bins

        df_col = df[column]
        ax = df_col.plot.hist(bins=bins,
                              alpha=0.7,
                              title=column,
                              legend=True,
                              figsize=(8, 6))
        fig = ax.get_figure()
        fig.savefig("static/plot.jpg")