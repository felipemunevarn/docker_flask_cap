#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 29 11:23:52 2021

@author: orc
"""

import pandas as pd
from data_s import Data

df  = pd.read_csv("iris.csv")

# max_f, min_f, sd_f, mean_f = data_p.resum_data()

# data_p.hist_data()

def testResumMethod(col):
    """
    >>> testResumMethod("sepal_length")
    (7.9, 4.3, 0.8, 5.8)

    >>> testResumMethod("sepal_width")
    (4.4, 2.0, 0.4, 3.1)
    
    >>> testResumMethod("petal_length")
    (6.9, 1.0, 1.8, 3.8)
    
    >>> testResumMethod("petal_width")
    (2.5, 0.1, 0.8, 1.2)
    """

    data_p = Data(df=df, column=col)
    return data_p.resum_data()

import doctest
doctest.testmod()
