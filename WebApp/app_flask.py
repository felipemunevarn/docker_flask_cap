#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 29 12:20:02 2021

@author: orc
"""

from flask import Flask, request, jsonify
from flask_wtf.csrf import CSRFProtect
from pandas import read_csv
from data_s import Data

app = Flask(__name__)
csrf = CSRFProtect()
csrf.init_app(app)

DF = read_csv('iris.csv')

@app.route('/')
def cal_hist():
    '''
    Display app

    Returns
    -------
    TYPE
        DESCRIPTION.

    '''

    column = str(request.args.get('column'))
    bins = int(request.args.get('bins'))

    data_p = Data(df=DF, column=column, bins=bins)

    max_f, min_f, sd_f, mean_f = data_p.resum_data()

    data_p.hist_data()

    data = {'min': min_f,
            'max': max_f,
            "std": sd_f,
            "mean": mean_f}

    return jsonify(data)

@app.route('/Hola')
def hola():
    return 'Hola'

if __name__ == "__main__":
    app.run(host='127.0.0.1', port=8000, debug=True)
