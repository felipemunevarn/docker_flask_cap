# Objetivo

El objetivo de esta prueba es generar la documentación necesaria para poder ejecutar el código y mostrar la solución a un usuario, el problema es poder generar una solución que permita analizar los datos de las columnas del conjunto de datos <https://es.wikipedia.org/wiki/Conjunto_de_datos_flor_iris> donde el usuario quiere analizar los datos estadísticos básicos (mínimo, máximo, desviación estándar y la media) de cada columna y poder ver la distribución de frecuencia de los datos en un histograma, para ello se creó una solución de la siguiente manera en la carpeta WebApp:

* data_s.py: este script se encarga de hacer los cálculos estadísticos y crea la visualización
* Test_data_s.py: este script prueba la clase y los métodos que esta en data_s.py
* iris.csv: este es un archivo de valores separados por comas donde se encuentran los datos
* app_flask.py: en este script se encuentra una ApiREST desarrollada en flask donde se pueden hacer peticiones sin tener un front end
* app_flask_front.py: en este script de flask permite renderizar un front end sencillo para hacer los análisis

Dentro de la carpeta WebApp/templates se encuentran los archivos html del front

* index.html: primera pantalla donde el usuario selecciona que columna quiere analizar y la cantidad de barras del histograma
* resultado.html: pantalla donde se muestran los resultados y la visualización

Dentro de la carpeta WebApp/static se almacena la imagen del histograma de la visualización
* plot.jpg: imagen de la visualización generada

# Información para ejecutar la aplicación en un contenedor

## docker_flask_ca 

En este proyecto se tiene el código para una capacitación donde se a borda de forma básica el framework Flask y Docker para desplegar

## Material
El material de la capacitación se puede consultar en: <https://docs.google.com/presentation/d/1OYUX9s3zD7xWPhe0JC4TjLehAxk8KiFrmBhlQG-3epY/edit?usp=sharing>

## Docker

### Crear el contenedor:
iris_app_docker es el nombre que se le da pueder cualquier otro
* `docker build -t iris_app_docker/uwsgi .`

### Desplegar el contenedor en local
* `docker run -u root -d -p 8080:8000 --restart unless-stopped -v $(pwd)/WebApp:/WebApp iris_app_docker/uwsgi`

### Login docker hub
* `docker login`

### Hacer tag de la imágen de Docker
docker tag local-image:tagname new-repo:tagname
* `docker tag iris_app_docker/uwsgi:latest docker_user/iris_app_docker:v1`

### Hacer push de la imagen
docker push new-repo:tagname
* `docker push docker_user/iris_app_docker:v1`

### Desplegar imagen clonada
* `docker run -d -p 8080:8000 -u root docker_user/iris_app_docker:v1`

### Hacer pull a la imagen
* `docker pull docker_user/iris_app_docker:v1`
