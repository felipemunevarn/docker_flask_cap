# Iris Data
## Documentation

Iris Data is your app to find all the stats you need about Iiris flower, specificly dimensions of petals and sepals:

- Max
- Min
- Mean
- ✨Standard Deviation ✨

## Prerequisites

- Docker
- Git

Without Docker, you still could run it, but also you need:

- Python

In case you don't have some of this, see:

- https://docs.docker.com/engine/install/
- https://git-scm.com/book/en/v2/Getting-Started-Installing-Git 
- https://www.python.org/downloads/

## Resources

Data was extracted from:

[Wikipedia article](https://en.wikipedia.org/wiki/Iris_flower_data_set) - Iris flower data set

App was done using:

[Flask](https://flask.palletsprojects.com/en/2.0.x/) - Web development, one drop at a time

## Installation

Clone the public repo:

```sh
git clone git@gitlab.com:oscar50/docker_flask_cap.git
cd docker_flask_cap
```

Build the image:

```sh
docker build -t <nameOfYourApp>/uwsgi .
```

Deploy the image on local:

```sh
docker run -u root -d -p 8080:8000 --restart unless-stopped -v $(pwd)/WebApp:/WebApp <nameOfYourApp>/uwsgi
```
Finally you could open the app in your browser using [localhost:8080](localhost:8080)

### Alternative

Also, you could use the app without Docker. After cloning the repo you need to create an environment in the folder WebApp:

```s
python3 -m venv venv
```

And then activate the virtual environment:

```s
. venv/bin/activate
```

Within the activated environment, use the following command to install the requirements:

```s
pip install -r requirements.txt
```

Finally it is possible to run the app with:

```s
python3 app_flask_front.py
```

## How to use the app

Just choose the parameter you want to evaluate and desired discretization (bins), so then you can obtain the max, min, mean and standard deviation and a graphic of this data as a plus.

![Screenshot](./img/screenshot.png)

## API Rest

You could use as API rest. First run the app without front:

```s
python3 app_flask.py
```

Now, you can send GET requests to:

http://localhost:8000/?column=sepal_width&bins=1

For "column" parameter you could specify one of this:

- sepal_length
- sepal_width
- petal_length
- petal_width

The field "bins" must be specified as an integer. It is not used but needed, do not omit it.

For example:

![Url preview](./img/url.png)

Sample API response:

![API response](./img/apiresponse.png)

For extended documentation of the API, visit:

https://documenter.getpostman.com/view/19412853/UVeDuTEX

## Modules Documentation

### app_flask_front.py
---------------------------------------
inicio()
    this funtion render form template
    
    Returns
    -------
    TYPE
        DESCRIPTION.
        
procesar()
    this function render UI basic
    
    Returns
    -------
    TYPE
        DESCRIPTION.

### app_flask.py        
---------------------------------------
cal_hist()
    Display app
    
    Returns
    -------
    TYPE
        DESCRIPTION.

### data_s.py
---------------------------------------
class Data(builtins.object)
 |  Data(df, column='sepal_length', bins=16)
 |  
 |  This class test dataframe iris
 |  
 |  Methods defined here:
 |  
 |  __init__(self, df, column='sepal_length', bins=16)
 |      Init variables
 |      
 |      Parameters
 |      ----------
 |      df : TYPE
 |          DESCRIPTION.
 |      column : TYPE, optional
 |          DESCRIPTION. The default is "sepal_length".
 |      bins : TYPE, optional
 |          DESCRIPTION. The default is 16.
 |      
 |      Returns
 |      -------
 |      None.
 |  
 |  hist_data(self)
 |      Plot selected column
 |      
 |      Returns
 |      -------
 |      None.
 |  
 |  resum_data(self)
 |      Calculate basic statistics
 |      
 |      Returns
 |      -------
 |      max_f : TYPE
 |          DESCRIPTION.
 |      min_f : TYPE
 |          DESCRIPTION.
 |      sd_f : TYPE
 |          DESCRIPTION.
 |      mean_f : TYPE
 |          DESCRIPTION.
 |  
 |  ----------------------------------------------------------------------
 |  Data descriptors defined here:
 |  
 |  __dict__
 |      dictionary for instance variables (if defined)
 |  
 |  __weakref__
 |      list of weak references to the object (if defined)
